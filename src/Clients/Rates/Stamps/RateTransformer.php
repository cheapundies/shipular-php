<?php

namespace Shipular\Clients\Rates\Stamps;

use Shipular\Models\Rate;

class RateTransformer
{
    const STAMPS_CODES_TO_LOCAL_CODES_MAPPING = [
        'US-PM'  => 'USPS_PRIORITY_MAIL',
        'US-FC'  => 'USPS_FIRST_CLASS_MAIL',
        'US-XM'  => 'USPS_EXPRESS_MAIL',
        'US-PP'  => 'USPS_PARCEL_POST',
        'US-LM'  => 'USPS_LIBRARY_MAIL',
        'US-MM'  => 'USPS_MEDIA_MAIL',
        'US-PS'  => 'USPS_PARCEL_SELECT',
        'US-FCI' => 'USPS_FIRST_CLASS_INTERNATIONAL',
        'US-PMI' => 'USPS_PRIORITY_MAIL_INTERNATIONAL',
        'US-EMI' => 'USPS_EXPRESS_MAIL_INTERNATIONAL',
    ];

    /**
     * @param array  $stampsRates
     * @return array
     */
    public function transform(array $stampsRates): array
    {
        $rates = [];

        foreach ($stampsRates as $rate) {
            if ($rate->PackageType === "Package") {
                $code = self::STAMPS_CODES_TO_LOCAL_CODES_MAPPING[$rate->ServiceType];

                $rates[] = new Rate('USPS', $code, $code, $rate->Amount, $rate->DeliverDays);
            }
        }

        return $rates;
    }
}

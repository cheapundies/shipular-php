<?php

namespace Shipular\Models;

class Rate implements \JsonSerializable
{
    /**
     * @var string
     */
    private $carrier;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $code;

    /**
     * @var float
     */
    private $cost;

    /**
     * @var string
     */
    private $daysInTransit;

    /**
     * Rate constructor.
     *
     * @param string  $description
     * @param string  $code
     * @param float   $cost
     * @param string  $daysInTransit
     */
    public function __construct(string $carrier, string $description, string $code, float $cost, string $daysInTransit)
    {
        $this->carrier       = $carrier;
        $this->description   = $description;
        $this->code          = $code;
        $this->cost          = $cost;
        $this->daysInTransit = $daysInTransit;
    }

    /**
     * @return string
     */
    public function getCarrier(): string
    {
        return $this->carrier;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @return float
     */
    public function getCost(): float
    {
        return $this->cost;
    }

    /**
     * @return string
     */
    public function getDaysInTransit(): string
    {
        return $this->daysInTransit;
    }

    public function jsonSerialize()
    {
        return [
            'carrier'         => $this->carrier,
            'description'     => $this->description,
            'code'            => $this->code,
            'cost'            => $this->cost,
            'days_in_transit' => $this->daysInTransit,
        ];
    }
}

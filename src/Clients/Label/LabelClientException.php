<?php

namespace Shipular\Clients\Label;

class LabelClientException extends \Exception
{
    /**
     * @param string  $message
     * @return LabelClientException
     */
    public static function clientError(string $message): LabelClientException
    {
        return new self($message);
    }
}

<?php

namespace Shipular\Clients\Rates\Stamps;

use Shipular\Clients\Label\Stamps\LabelClient;
use Shipular\Models\Rate;
use Shipular\CLients\Rates\RateClient as RateClientInterface;
use Shipular\Requests\Rates\RateRequest;
use SoapClient as NativeSoapClient;

class RateClient extends NativeSoapClient implements RateClientInterface
{
    /**
     * RateClient constructor.
     */
    public function __construct()
    {
        parent::__construct(__DIR__ . '/../../../resources/Stamps/swsimv68.wsdl', [
            'exceptions' => 0,
            'trace'      => true,
        ]);
    }

    /**
     * @inheritdoc
     */
    public function getRates(RateRequest $request): array
    {
        $rates = $this->fetchRates($request);

        return (new RateTransformer())->transform($rates);
    }

    /**
     * @param RateRequest  $request
     * @return array
     */
    private function fetchRates(RateRequest $request): array
    {
        $fromAddress = $request->getFromAddress();
        $toAddress   = $request->getToAddress();
        $package     = $request->getPackage();

        $request = [
            'Credentials' => [
                'IntegrationID' => 'e13dde83-59b9-4b45-9a51-3f83016fd883',
                'Username'      => 'whoamedia',
                'Password'      => '784bc2lz',
            ],
            'Rate' => [
                'FromZIPCode' => $fromAddress->getPostalCode(),
                'ToZIPCode'   => $toAddress->getPostalCode(),
                'ToCountry'   => $toAddress->getCountry(),
                'WeightLb'    => $package->getWeight(),
                'Length'      => $package->getLength(),
                'Width'       => $package->getWidth(),
                'Height'      => $package->getHeight(),
                'ShipDate'    => date('Y-m-d'),
                'PackageType' => 'Package',
                'AddOns'      => [
                    [
                        'AddOnType' => 'SC-A-HP',
                    ],
                ],
            ]
        ];

        if (!in_array($toAddress->getCompany(),  LabelClient::COUNTRIES_WITH_NO_DELIVERY_CONFIRMATION)) {
            $request['AddOns'][] = [
                'AddOnType' => 'US-A-DC',
            ];
        }

        return $this->__soapCall('GetRates', [$request])->Rates->Rate;
    }
}

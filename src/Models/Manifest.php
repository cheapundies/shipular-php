<?php

namespace Shipular\Models;

class Manifest
{
    /**
     * @var string
     */
    private $manfiestData;

    /**
     * Manifest constructor.
     *
     * @param string  $manifestData
     */
    public function __construct(string $manifestData)
    {
        $this->manfiestData = $manifestData;
    }

    /**
     * @return string
     */
    public function getManifestData(): string
    {
        return $this->manfiestData;
    }
}

<?php

namespace Shipular\Clients\Manifest\Stamps;

use Shipular\Clients\Manifest\ManifestClient as ManifestClientInterface;
use Shipular\Clients\Manifest\ManifestClientException;
use Shipular\Models\Manifest;
use Shipular\Requests\Manifest\ManifestRequest;

class ManifestClient extends \SoapClient implements ManifestClientInterface
{
    /**
     * LabelClient constructor.
     */
    public function __construct()
    {
        parent::__construct(__DIR__ . '/../../../resources/Stamps/swsimv68.wsdl', [
            'exceptions' => 0,
            'trace'      => true,
        ]);
    }

    /**
     * @inheritdoc
     */
    public function createManifest(ManifestRequest $manifestRequest): Manifest
    {
        $address = $manifestRequest->getFromAddress();

        $scanform = $this->CreateScanForm([
            'Credentials' => [
                'IntegrationID' => 'e13dde83-59b9-4b45-9a51-3f83016fd883',
                'Username'      => 'whoamedia',
                'Password'      => '784bc2lz',
            ],
            'FromAddress' => [
                'FullName'      => $address->getName(),
                'Company'       => $address->getCompany(),
                'Address1'      => $address->getAddress1(),
                'Address2'      => $address->getAddress2(),
                'City'          => $address->getCity(),
                'ZIPCode'       => $address->getPostalCode(),
                'State'         => $address->getProvince(),
            ],
            'ShipDate'          => $manifestRequest->getShippedDate()->format('Y-m-d'),
            'ImageType'         => 'Pdf',
            'PrintInstructions' => false
        ]);

        if ($scanform instanceof \SoapFault) {
            throw new ManifestClientException($scanform->faultstring);
        }

        $scanFormData = $this->getScanformDataFromResult($scanform);

        return new Manifest($scanFormData);
    }

    /**
     * @param $scanformResult
     * @return string
     */
    private function getScanformDataFromResult($scanformResult): string
    {
        $scanformData = file_get_contents($scanformResult->Url);

        return base64_encode($scanformData);
    }
}

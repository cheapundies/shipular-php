<?php

namespace Shipular\Clients\Label\UPS;

use Shipular\Models\Label;

class LabelTransformer
{
    /**
     * @param \stdClass  $shipmentResult  Response from UPS SOAP call
     * @return Label
     */
    public function transform(\stdClass $shipmentResult): Label
    {
        $result = $shipmentResult->ShipmentResults;

        $trackingCode = $result->PackageResults->TrackingNumber;
        $labelData    = $result->PackageResults->ShippingLabel->GraphicImage;
        $cost         = $result->ShipmentCharges->TotalCharges->MonetaryValue;

        return new Label($trackingCode, $labelData, $cost);
    }
}

<?php

namespace Shipular\Clients\Label\Stamps;

use Shipular\Models\Label;

class LabelTransformer
{
    /**
     * @param \stdClass  $shipmentResult  Response from Stamps SOAP call
     * @return Label
     */
    public function transform(\stdClass $shipmentResult): Label
    {
        return new Label($shipmentResult->TrackingNumber, base64_encode($shipmentResult->ImageData->base64Binary), 0);
    }
}

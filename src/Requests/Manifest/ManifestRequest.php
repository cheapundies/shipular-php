<?php

namespace Shipular\Requests\Manifest;

use Shipular\Models\Address;

class ManifestRequest
{
    /**
     * @var Address
     */
    private $fromAddress;

    /**
     * @var \DateTime
     */
    private $shippedDate;

    /**
     * ManifestRequest constructor.
     *
     * @param Address    $fromAddress
     * @param \DateTime  $shippedDate
     */
    public function __construct(Address $fromAddress, \DateTime $shippedDate)
    {
        $this->fromAddress = $fromAddress;
        $this->shippedDate = $shippedDate;
    }

    /**
     * @return Address
     */
    public function getFromAddress(): Address
    {
        return $this->fromAddress;
    }

    /**
     * @return \DateTime
     */
    public function getShippedDate(): \DateTime
    {
        return $this->shippedDate;
    }
}

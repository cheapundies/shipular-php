<?php

namespace Shipular\Requests\Rates;

use Shipular\Models\Address;
use Shipular\Models\Package;

class RateRequest
{
    /**
     * @var Address
     */
    private $toAddress;

    /**
     * @var Address
     */
    private $fromAddress;

    /**
     * @var Package
     */
    private $package;

    /**
     * RateRequest constructor.
     *
     * @param Address  $fromAddress
     * @param Address  $toAddress
     * @param Package  $package
     */
    public function __construct(Address $fromAddress, Address $toAddress, Package $package)
    {
        $this->toAddress   = $toAddress;
        $this->fromAddress = $fromAddress;
        $this->package     = $package;
    }

    /**
     * @return Address
     */
    public function getToAddress(): Address
    {
        return $this->toAddress;
    }

    /**
     * @return Address
     */
    public function getFromAddress(): Address
    {
        return $this->fromAddress;
    }

    /**
     * @return Package
     */
    public function getPackage(): Package
    {
        return $this->package;
    }
}

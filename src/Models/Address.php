<?php

namespace Shipular\Models;

class Address
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $company;

    /**
     * @var string
     */
    private $address1;

    /**
     * @var string
     */
    private $city;

    /**
     * @var string
     */
    private $province;

    /**
     * @var string
     */
    private $country;

    /**
     * @var string
     */
    private $postalCode;

    /**
     * @var string
     */
    private $phoneNumber;

    /**
     * Address constructor.
     *
     * @param string  $name
     * @param string  $company
     * @param string  $address1
     * @param string  $address2
     * @param string  $city
     * @param string  $province
     * @param string  $country
     * @param string  $postalCode
     * @param string  $phoneNumber
     */
    public function __construct(
        string $name,
        string $company,
        string $address1,
        string $address2,
        string $city,
        string $province,
        string $country,
        string $postalCode,
        string $phoneNumber = ''
    ) {
        $this->name        = $name;
        $this->company     = $company;
        $this->address1    = $address1;
        $this->address2    = $address2;
        $this->city        = $city;
        $this->province    = $province;
        $this->country     = $country;
        $this->postalCode  = $postalCode;
        $this->phoneNumber = $phoneNumber;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getCompany(): string
    {
        return $this->company;
    }

    /**
     * @return string
     */
    public function getAddress1(): string
    {
        return $this->address1;
    }

    /**
     * @return string
     */
    public function getAddress2(): string
    {
        return $this->address2;
    }

    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * @return string
     */
    public function getProvince(): string
    {
        return $this->province;
    }

    /**
     * @return string
     */
    public function getCountry(): string
    {
        return $this->country;
    }

    /**
     * @return string
     */
    public function getPostalCode(): string
    {
        return $this->postalCode;
    }

    /**
     * @return string
     */
    public function getPhoneNumber(): string
    {
        return $this->phoneNumber;
    }
}

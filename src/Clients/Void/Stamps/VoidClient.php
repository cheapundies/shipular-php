<?php

namespace Shipular\Clients\Void\Stamps;

use Shipular\Clients\Void\VoidClient as VoidClientInterface;

class VoidClient extends \SoapClient implements VoidClientInterface
{
    /**
     * LabelClient constructor.
     */
    public function __construct()
    {
        parent::__construct(__DIR__ . '/../../../resources/Stamps/swsimv68.wsdl', [
            'exceptions' => 0,
            'trace'      => true,
        ]);
    }

    /**
     * @inheritDoc
     */
    public function voidLabel(string $trackingCode)
    {
        $this->CancelIndicium([
            'Credentials' => [
                'IntegrationID' => 'e13dde83-59b9-4b45-9a51-3f83016fd883',
                'Username'      => 'whoamedia',
                'Password'      => '784bc2lz',
            ],
            'TrackingNumber' => $trackingCode
        ]);
    }
}

<?php

namespace Shipular\CLients\Rates;

use Shipular\Requests\Rates\RateRequest;

interface RateClient
{
    /**
     * @param RateRequest  $request
     * @return array
     */
    public function getRates(RateRequest $request): array;
}

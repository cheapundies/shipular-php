<?php

namespace Tests;

use Shipular\Clients\Label\Stamps\LabelClient as StampsLabelClient;
use Shipular\Clients\Label\UPS\LabelClient as UPSLabelClient;
use Shipular\Models\Address;
use Shipular\Models\CustomItem;
use Shipular\Models\Package;
use Shipular\Models\Rate;
use Shipular\Requests\Label\LabelRequest;
use Tests\TestFixtures\TestCase;

class LabelTest extends TestCase
{
    private $toAddress;

    private $fromAddress;

    protected function setUp()
    {
        parent::setUp();

        $this->toAddress = new Address('Ryon Hunter', '', '203 Hull St', '', 'Richmond', 'VA', 'US', '23224');

        $this->fromAddress = new Address('Shipping Department', 'CheapUndies', '9845 Atlee Commons Drive', '', 'Ashland', 'VA', 'US', '23005');
    }

    /**
     * @test
     */
    public function it_gets_ups_label()
    {
        $client = new UPSLabelClient;

        $package = new Package(1.0, 11.0, 11.0, 11.0);
        $rate = new Rate('UPS', 'UPS_GROUND', 'UPS_GROUND', 0.00, '');

        $label = $client->buyLabel(new LabelRequest($this->fromAddress, $this->toAddress, $package, $rate));

        // assert that the tracking code really is from UPS
        $this->assertEquals('1Z', substr($label->getTrackingCode(), 0, 2));
    }

    /**
     * @test
     */
    public function it_gets_stamps_label()
    {
        $client = new StampsLabelClient;

        $package = new Package(0.8, 11.0, 11.0, 11.0);
        $rate = new Rate('USPS', 'USPS_FIRST_CLASS_MAIL', 'USPS_FIRST_CLASS_MAIL', 0.00, '');

        $label = $client->buyLabel(new LabelRequest($this->fromAddress, $this->toAddress, $package, $rate));

        // assert that the tracking code really is from Stamps
        $this->assertEquals('9', substr($label->getTrackingCode(), 0, 1));
    }

    /**
     * @test
     */
    public function it_gets_international_stamps_label()
    {
        $client = new StampsLabelClient;

        $package = new Package(0.8, 11.0, 11.0, 11.0);
        $rate = new Rate('USPS', 'USPS_FIRST_CLASS_INTERNATIONAL', 'USPS_FIRST_CLASS_INTERNATIONAL', 0.00, '');

        $toAddress = new Address('Michal Szczuko', '', 'Zifling 53', '', 'Willmering', '', 'DE', '93497', '8042972731');

        $label = $client->buyLabel(new LabelRequest($this->fromAddress, $toAddress, $package, $rate, [
            new CustomItem('A Good Pair Of Undies', 2, 11.95, 0.4),
            new CustomItem('A Great Pair Of Undies', 2, 16.95, 0.4),
        ]));

        // assert that the tracking code really is from Stamps
        $this->assertEquals('LJ', substr($label->getTrackingCode(), 0, 2));
    }
}

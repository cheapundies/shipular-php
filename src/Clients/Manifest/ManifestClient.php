<?php

namespace Shipular\Clients\Manifest;

use Shipular\Models\Manifest;
use Shipular\Requests\Manifest\ManifestRequest;

interface ManifestClient
{
    /**
     * @param ManifestRequest  $manifestRequest
     * @return Manifest
     */
    public function createManifest(ManifestRequest $manifestRequest): Manifest;
}

<?php

namespace Shipular\Clients\Rates\UPS;

use Shipular\Models\Rate;

class RateTransformer
{
    const UPS_CODE_TO_LOCAL_CODE_MAPPING = [
        '01' => 'UPS_NEXT_DAY_AIR',
        '02' => 'UPS_TWO_DAY_AIR',
        '03' => 'UPS_GROUND',
        '07' => 'UPS_WORLDWIDE_EXPRESS',
        '08' => 'UPS_WORLDWIDE_EXPEDITED',
        '12' => 'UPS_THREE_DAY_SELECT',
        '13' => 'UPS_NEXT_DAY_AIR_SAVER',
        '14' => 'UPS_NEXY_DAY_AIR_EARLY_AM',
        '59' => 'UPS_TWO_DAY_AIR_EARLY_AM',
        '65' => 'UPS_WORLDWIDE_SAVER',
    ];

    /**
     * @param array  $upsRates
     * @return array
     */
    public function transform(array $upsRates): array
    {
        $rates = [];

        foreach ($upsRates as $rate) {
            $code = self::UPS_CODE_TO_LOCAL_CODE_MAPPING[$rate->Service->Code];

            $rates[] = new Rate('UPS', $code, $code, $rate->TotalCharges->MonetaryValue, '');
        }

        return $rates;
    }
}

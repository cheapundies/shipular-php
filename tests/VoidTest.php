<?php

namespace Tests;

use Shipular\Clients\Void\Stamps\VoidClient as StampsVoidClient;
use Shipular\Clients\Void\UPS\VoidClient as UPSVoidClient;
use Tests\TestFixtures\TestCase;

class VoidTest extends TestCase
{
    /**
     * @test
     */
    public function it_voids_ups_label()
    {
        $client = new UPSVoidClient;

        $client->voidLabel('1Z12345E0390856432');
    }

    /**
     * @test
     */
    public function it_voids_stamps_label()
    {
        $client = new StampsVoidClient;

        $client->voidLabel('9434283291384913948');
    }
}

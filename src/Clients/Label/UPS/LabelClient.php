<?php

namespace Shipular\Clients\Label\UPS;

use Shipular\Clients\Label\LabelClient as LabelClientInterface;
use Shipular\Clients\Label\LabelClientException;
use Shipular\Clients\Rates\UPS\RateTransformer;
use Shipular\Models\Label;
use Shipular\Requests\Label\LabelRequest;
use SoapClient as NativeSoapClient;

class LabelClient extends NativeSoapClient implements LabelClientInterface
{
    /**
     * LabelClient constructor.
     */
    public function __construct()
    {
        parent::__construct(__DIR__ . '/../../../resources/UPS/Ship.wsdl', [
            'exceptions' => 0,
            'trace'      => true,
        ]);

        $header = new \SoapHeader('http://www.ups.com/XMLSchema/XOLTWS/UPSS/v1.0', 'UPSSecurity', [
            'UsernameToken' => [
                'Username' => 'smst-rhunter',
                'Password' => 'Changeme14!',
            ],
            'ServiceAccessToken' => [
                'AccessLicenseNumber' => 'DD2EC4DE9C859D18'
            ]
        ]);

        $this->__setSoapHeaders($header);
    }

    /**
     * @inheritDoc
     */
    public function buyLabel(LabelRequest $request): Label
    {
        $response = $this->purchaseShipment($request);

        return (new LabelTransformer())->transform($response);
    }

    /**
     * @param LabelRequest  $request
     * @return \stdClass
     */
    private function purchaseShipment(LabelRequest $request): \stdClass
    {
        $toAddress   = $request->getToAddress();
        $fromAddress = $request->getFromAddress();
        $package     = $request->getPackage();
        $rate        = $request->getRate();

        $upsServiceCodes = array_flip(RateTransformer::UPS_CODE_TO_LOCAL_CODE_MAPPING);

        $upsServiceCode = $upsServiceCodes[$rate->getCode()];

        $response = $this->ProcessShipment([
            'Request' => [
                'TransactionReference' => [
                    // Customer Context is meant more for debugging purposes
                    'CustomerContext' => 'Foo',
                    'XpciVersion'     => '1.0',
                ],
                // Do not validate addresses. Alternatively, you can change this to validate addresses.
                'RequestOption' => 'nonvalidate',
            ],
            'Shipment' => [
                'Shipper' => [
                    'ShipperNumber' => 'F525Y8',
                    'Name' => $fromAddress->getName(),
                    'Address' => [
                        'AddressLine' => [
                            $fromAddress->getAddress1(),
                            $fromAddress->getAddress2(),
                        ],
                        'City'              => $fromAddress->getCity(),
                        'StateProvinceCode' => $fromAddress->getProvince(),
                        'PostalCode'        => $fromAddress->getPostalCode(),
                        'CountryCode'       => $fromAddress->getCountry(),
                    ],
                ],

                'ShipTo' => [
                    'ShipperNumber' => 'F525Y8',
                    'Name'          => $toAddress->getName(),
                    'Address' => [
                        'AddressLine' => [
                            $toAddress->getAddress1(),
                            $toAddress->getAddress2(),
                        ],
                        'City'              => $toAddress->getCity(),
                        'StateProvinceCode' => $toAddress->getProvince(),
                        'PostalCode'        => $toAddress->getPostalCode(),
                        'CountryCode'       => $toAddress->getCountry(),
                    ],
                ],

                'PaymentInformation' => [
                    'ShipmentCharge' => [
                        'Type'        => '01',
                        'BillShipper' => [
                            'AccountNumber' => 'F525Y8',
                        ],
                    ],
                ],
                'Service' => [
                    'Code' => $upsServiceCode,
                ],
                'Description' => $package->getMemo(),
                'Package' => [
                    'Packaging' => [
                        'Code'  => '02',
                    ],
                    'PackageWeight' => [
                        'UnitOfMeasurement' => [
                            'Code'        => 'LBS',
                            'Description' => 'LBS',
                        ],
                        'Weight' => $package->getWeight(),
                    ]
                ]
            ],
            'LabelSpecification' => [
                'LabelImageFormat' => [
                    // Could be GIF, ZPL, PNG, or PDF.
                    'Code'        => 'ZPL',
                    'Description' => 'ZPL',
                ],
                'LabelStockSize' => [
                    'Height' => '6',
                    'Width'  => '4',
                ],
            ],
        ]);

        if ($response instanceof \SoapFault) {
            throw LabelClientException::clientError($response->detail->Errors->ErrorDetail->PrimaryErrorCode->Description);
        }

        return $response;
    }
}

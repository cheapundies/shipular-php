<?php

namespace Shipular\Requests\Label;

use Shipular\Models\Address;
use Shipular\Models\Package;
use Shipular\Models\Rate;

class LabelRequest
{
    /**
     * @var Address
     */
    private $toAddress;

    /**
     * @var Address
     */
    private $fromAddress;

    /**
     * @var Package
     */
    private $package;

    /**
     * @var Rate
     */
    private $rate;

    /**
     * @var CustomItem[]
     */
    private $customItems;

    /**
     * RateRequest constructor.
     *
     * @param Address  $fromAddress
     * @param Address  $toAddress
     * @param Package  $package
     * @param Rate     $package
     * @param array    $customItems
     */
    public function __construct(Address $fromAddress, Address $toAddress, Package $package, Rate $rate, array $customItems = [])
    {
        $this->toAddress   = $toAddress;
        $this->fromAddress = $fromAddress;
        $this->package     = $package;
        $this->rate        = $rate;
        $this->customItems = $customItems;
    }

    /**
     * @return Address
     */
    public function getToAddress(): Address
    {
        return $this->toAddress;
    }

    /**
     * @return Address
     */
    public function getFromAddress(): Address
    {
        return $this->fromAddress;
    }

    /**
     * @return Package
     */
    public function getPackage(): Package
    {
        return $this->package;
    }

    /**
     * @return Rate
     */
    public function getRate(): Rate
    {
        return $this->rate;
    }

    /**
     * @return CustomItem[]
     */
    public function getCustomsItems(): array
    {
        return $this->customItems;
    }
}

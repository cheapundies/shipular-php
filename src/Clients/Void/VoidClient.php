<?php

namespace Shipular\Clients\Void;

interface VoidClient
{
    /**
     * @param string  $trackingCode
     * @return mixed
     */
    public function voidLabel(string $trackingCode);
}

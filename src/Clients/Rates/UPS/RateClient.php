<?php

namespace Shipular\Clients\Rates\UPS;

use Shipular\Models\Rate;
use Shipular\CLients\Rates\RateClient as RateClientInterface;
use Shipular\Requests\Rates\RateRequest;
use SoapClient as NativeSoapClient;

class RateClient extends NativeSoapClient implements RateClientInterface
{
    /**
     * RateClient constructor.
     */
    public function __construct()
    {
        parent::__construct(__DIR__ . '/../../../resources/UPS/Rate.wsdl', [
            'exceptions' => 0,
            'trace'      => true,
        ]);

        $header = new \SoapHeader('http://www.ups.com/XMLSchema/XOLTWS/UPSS/v1.0', 'UPSSecurity', [
            'UsernameToken' => [
                'Username' => 'smst-rhunter',
                'Password' => 'Changeme14!',
            ],
            'ServiceAccessToken' => [
                'AccessLicenseNumber' => 'DD2EC4DE9C859D18'
            ]
        ]);

        $this->__setSoapHeaders($header);
    }

    /**
     * @inheritdoc
     */
    public function getRates(RateRequest $request): array
    {
        $rates = $this->fetchRates($request);

        return (new RateTransformer())->transform($rates);
    }

    /**
     * @param RateRequest  $request
     * @return mixed
     */
    private function fetchRates(RateRequest $request)
    {
        $toAddress   = $request->getToAddress();
        $fromAddress = $request->getFromAddress();
        $package     = $request->getPackage();

        if ($toAddress->getCountry() != 'US') {
            return [];
        }

        $response = $this->ProcessRate([
            'Request' => [
                'RequestOption' => 'Shop',
            ],
            'Shipment' => [
                'PickUpType' => [
                    // @TODO: make this programmable
                    'Code'        => '01',
                    'Description' => 'Daily Pickup',
                ],
                'Shipper' => [
                    // @TODO: make this programmable
                    'ShipperNumber' => 'F525Y8',
                    'Name'          => $fromAddress->getName(),
                    'Address' => [
                        'AddressLine' => [
                            $fromAddress->getAddress1(),
                            $fromAddress->getAddress2(),
                        ],
                        'City'              => $fromAddress->getCity(),
                        'StateProvinceCode' => $fromAddress->getProvince(),
                        'PostalCode'        => $fromAddress->getPostalCode(),
                        'CountryCode'       => $fromAddress->getCountry(),
                    ],
                ],
                'ShipTo' => [
                    'Name'    => $toAddress->getName(),
                    'Address' => [
                        'AddressLine' => [
                            $toAddress->getAddress1(),
                            $toAddress->getAddress2(),
                        ],
                        'City'              => $toAddress->getCity(),
                        'StateProvinceCode' => $toAddress->getProvince(),
                        'PostalCode'        => $toAddress->getPostalCode(),
                        'CountryCode'       => $toAddress->getCountry(),
                    ],
                ],
                'PaymentInformation' => [
                    'ShipmentCharge' => [
                        // @TODO: make this programmable
                        'Type'        => '01',
                        'BillShipper' => [
                            'AccountNumber' => 'F525Y8',
                        ],
                    ],
                ],
                'Package' => [
                    'PackagingType' => [
                        // @TODO: make this programmable
                        'Code' => '02',
                    ],
                    'PackageWeight' => [
                        'UnitOfMeasurement' => [
                            'Code'        => 'LBS',
                            'Description' => 'LBS',
                        ],
                        'Weight' => $package->getWeight(),
                    ],
                    'Dimensions' => [
                        'UnitOfMeasurement' => [
                            'Code'        => 'IN',
                            'Description' => 'IN',
                        ],
                        'Length' => $package->getLength(),
                        'Width'  => $package->getWidth(),
                        'Height' => $package->getHeight(),
                    ],
                ]
            ]
        ]);

        if (!property_exists($response, 'RatedShipment')) {
            throw new \Exception($response->detail->Errors->ErrorDetail->PrimaryErrorCode->Description);
        }

        return $response->RatedShipment;
    }
}

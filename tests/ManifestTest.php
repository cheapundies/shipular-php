<?php

namespace Tests;

use Shipular\Clients\Manifest\Stamps\ManifestClient as StampsManifestClient;
use Shipular\Models\Address;
use Shipular\Requests\Manifest\ManifestRequest;
use Tests\TestFixtures\TestCase;

class ManifestTest extends TestCase
{
    /**
     * @test
     */
    public function it_gets_stamps_manifest()
    {
        $client = new StampsManifestClient;

        $manifest = $client->createManifest(new ManifestRequest(new Address(
            'John Doe',
            '123 Inc',
            '123 Main St',
            '',
            'Richmond',
            'VA',
            'US',
            '23224'
        ), new \DateTime));

        $this->assertNotNull($manifest->getManifestData());
    }
}

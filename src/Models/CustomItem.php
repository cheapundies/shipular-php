<?php

namespace Shipular\Models;

class CustomItem implements \JsonSerializable
{
    /**
     * @var string
     */
    private $description;

    /**
     * @var int
     */
    private $quantity;

    /**
     * @var float
     */
    private $value;

    /**
     * @var float
     */
    private $weight;

    /**
     * Rate constructor.
     *
     * @param string  $description
     * @param int     $quantity
     * @param float   $value
     * @param float   $weight
     */
    public function __construct(string $description, int $quantity, float $value, float $weight)
    {
        $this->description = $description;
        $this->quantity    = $quantity;
        $this->value       = $value;
        $this->weight      = $weight;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @return float
     */
    public function getValue(): float
    {
        return $this->value;
    }

    /**
     * @return float
     */
    public function getWeight(): float
    {
        return $this->weight;
    }

    public function jsonSerialize()
    {
        return [
            'description' => $this->description,
            'quantity'    => $this->quantity,
            'value'       => $this->value,
        ];
    }
}

<?php

namespace Shipular\Models;

class Label implements \JsonSerializable
{
    /**
     * @var string
     */
    private $trackingCode;

    /**
     * @var string
     */
    private $labelData;

    /**
     * @var float
     */
    private $cost;

    /**
     * Label constructor.
     *
     * @param string  $trackingCode
     * @param string  $labalData
     * @param float   $cost
     */
    public function __construct(string $trackingCode, string $labalData, float $cost)
    {
        $this->trackingCode = $trackingCode;
        $this->labelData    = $labalData;
        $this->cost         = $cost;
    }

    /**
     * @return string
     */
    public function getTrackingCode(): string
    {
        return $this->trackingCode;
    }

    /**
     * @return string
     */
    public function getLabelData(): string
    {
        return $this->labelData;
    }

    /**
     * @return float
     */
    public function getCost(): float
    {
        return $this->cost;
    }

    public function jsonSerialize()
    {
        return [
            'tracking_code' => $this->getTrackingCode(),
            'label_data'    => $this->getLabelData(),
            'cost'          => $this->getCost(),
        ];
    }
}

<?php

namespace Shipular\Clients\Label;

use Shipular\Models\Label;
use Shipular\Requests\Label\LabelRequest;

interface LabelClient
{
    /**
     * @param LabelRequest  $request
     * @return Label
     */
    public function buyLabel(LabelRequest $request): Label;
}

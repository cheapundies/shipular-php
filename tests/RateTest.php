<?php

namespace Tests;

use Shipular\Clients\Rates\Stamps\RateClient as StampsRateClient;
use Shipular\Clients\Rates\UPS\RateClient as UPSRateClient;
use Shipular\Models\Address;
use Shipular\Models\Package;
use Shipular\Models\Rate;
use Shipular\Requests\Rates\RateRequest;
use Tests\TestFixtures\TestCase;

class RateTest extends TestCase
{
    /**
     * @test
     */
    public function it_gets_ups_rates()
    {
        $client = new UPSRateClient;

        $to = new Address('Ryon Hunter', '', '203 Hull St', '', 'Richmond', 'VA', 'US', '23224');
        $from = new Address('Shipping Department', 'CheapUndies', '9845 Atlee Commons Drive', '', 'Ashland', 'VA', 'US', '23005');
        $package = new Package(1.0, 11.0, 11.0, 11.0);

        $rates = $client->getRates(new RateRequest($from, $to, $package));

        $this->assertInstanceOf(Rate::class, $rates[0]);
    }

    /**
     * @test
     */
    public function it_gets_stamps_rates()
    {
        $client = new StampsRateClient;

        $to = new Address('Ryon Hunter', '','203 Hull St', '', 'Richmond', 'VA', 'US', '23224');
        $from = new Address('Shipping Department', 'CheapUndies', '9845 Atlee Commons Drive', '', 'Ashland', 'VA', 'US', '23005');
        $package = new Package(1.0, 11.0, 11.0, 11.0);

        $rates = $client->getRates(new RateRequest($from, $to, $package));

        $this->assertInstanceOf(Rate::class, $rates[0]);
    }

    /**
     * @test
     */
    public function it_gets_international_stamps_rates()
    {
        $client = new StampsRateClient;

        $to = new Address('Michal Szczuko', '','Zifling 53', '', 'Willmering', '', 'DE', '93497');
        $from = new Address('Shipping Department', 'CheapUndies', '9845 Atlee Commons Drive', '', 'Ashland', 'VA', 'US', '23005');

        $package = new Package(1.0, 11.0, 11.0, 11.0);

        $rates = $client->getRates(new RateRequest($from, $to, $package));

        $this->assertInstanceOf(Rate::class, $rates[0]);
    }
}

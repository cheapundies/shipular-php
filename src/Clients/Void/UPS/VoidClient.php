<?php

namespace Shipular\Clients\Void\UPS;

use Shipular\Clients\Void\VoidClient as VoidClientInterface;

class VoidClient extends \SoapClient implements VoidClientInterface
{
    /**
     * VoidClient constructor.
     */
    public function __construct()
    {
        parent::__construct(__DIR__ . '/../../../resources/UPS/Void.wsdl', [
            'exceptions' => 0,
            'trace'      => true,
        ]);

        $header = new \SoapHeader('http://www.ups.com/XMLSchema/XOLTWS/UPSS/v1.0', 'UPSSecurity', [
            'UsernameToken' => [
                'Username' => 'smst-rhunter',
                'Password' => 'Changeme14!',
            ],
            'ServiceAccessToken' => [
                'AccessLicenseNumber' => 'DD2EC4DE9C859D18'
            ]
        ]);

        $this->__setSoapHeaders($header);
    }

    /**
     * @inheritdoc
     */
    public function voidLabel(string $trackingCode)
    {
        $this->ProcessVoid([
            'Request' => [
                'TransactionReference' => [
                    'RequestOption' => '',
                ],
            ],
            'VoidShipment' => [
                'ShipmentIdentificationNumber' => $trackingCode,
                'TrackingCode'                 => $trackingCode,
            ]
        ]);
    }
}

<?php

namespace Shipular\Clients\Label\Stamps;

use Shipular\Clients\Label\LabelClient as LabelClientInterface;
use Shipular\Clients\Label\LabelClientException;
use Shipular\Clients\Rates\Stamps\RateTransformer;
use Shipular\Models\Label;
use Shipular\Requests\Label\LabelRequest;
use SoapClient as NativeSoapClient;

class LabelClient extends NativeSoapClient implements LabelClientInterface
{
    const COUNTRIES_WITH_NO_DELIVERY_CONFIRMATION = [
        'CZ',
        'NO',
        'IE',
        'RO',
        'SE',
        'BG',
        'CY',
        'JP'
    ];

    /**
     * LabelClient constructor.
     */
    public function __construct()
    {
        parent::__construct(__DIR__ . '/../../../resources/Stamps/swsimv68.wsdl', [
            'exceptions' => 0,
            'trace'      => true,
        ]);
    }

    /**
     * @inheritDoc
     */
    public function buyLabel(LabelRequest $request): Label
    {
        $response = $this->purchaseLabel($request);

        return (new LabelTransformer)->transform($response);
    }

    private function purchaseLabel(LabelRequest $request): \stdClass
    {
        $fromAddress = $request->getFromAddress();
        $toAddress   = $request->getToAddress();
        $package     = $request->getPackage();
        $rate        = $request->getRate();

        $stampsRates = array_flip(RateTransformer::STAMPS_CODES_TO_LOCAL_CODES_MAPPING);

        $stampsRate = $stampsRates[$rate->getCode()];

        // @TODO: handle this better
        $toName = explode(' ', $toAddress->getName());

        if (count($toName) > 1) {
            list($firstName, $lastName) = $toName;
        } else {
            $firstName = $toName[0];
            $lastName = '';
        }

        $labelRequest = [
            'Credentials' => [
                'IntegrationID' => 'e13dde83-59b9-4b45-9a51-3f83016fd883',
                'Username'      => 'whoamedia',
                'Password'      => '784bc2lz',
            ],
            'IntegratorTxID'  => microtime(true),
            'Rate' => [
                'FromZIPCode' => $fromAddress->getPostalCode(),
                'ToZIPCode'   => $toAddress->getPostalCode(),
                'ToCountry'   => $toAddress->getCountry(),
                'WeightLb'    => $package->getWeight(),
                'ShipDate'    => date('Y-m-d'),
                'ServiceType' => $stampsRate,
                'PackageType' => 'Package',
            ],
            'From' => [
                'FullName'    => $fromAddress->getName(),
                'Company'     => $fromAddress->getCompany(),
                'Address1'    => $fromAddress->getAddress1(),
                'Address2'    => $fromAddress->getAddress2(),
                'City'        => $fromAddress->getCity(),
                'State'       => $fromAddress->getProvince(),
                'ZIPCode'     => $fromAddress->getPostalCode(),
                'Country'     => $fromAddress->getCountry(),
                'PhoneNumber' => $fromAddress->getPhoneNumber(),
            ],
            'To' => [
                'FirstName'   => $firstName,
                'LastName'    => $lastName,
                'Address1'    => $toAddress->getAddress1(),
                'Address2'    => $toAddress->getAddress2(),
                'City'        => $toAddress->getCity(),
                'ZIPCode'     => $toAddress->getPostalCode(),
                'State'       => $toAddress->getProvince(),
                'Country'     => $toAddress->getCountry(),
                'PhoneNumber' => $toAddress->getPhoneNumber(),
            ],
            'ImageType'       => 'Zpl',
            'ReturnImageData' => true,
            'AddOns'          => [
                [
                    'AddOnType' => 'SC-A-HP',
                ],
            ],
            'memo'      => $package->getMemo(),
            'printMemo' => true,
        ];

        if (!in_array($toAddress->getCompany(), self::COUNTRIES_WITH_NO_DELIVERY_CONFIRMATION)) {
            $labelRequest['AddOns'][] = [
                'AddOnType' => 'US-A-DC',
            ];
        }

        if ($toAddress->getCountry() != 'US') {
            $labelRequest['Customs'] = [
                'ContentType'   => 'Merchandise',
                'CustomsSigner' => 'Edward Upton',
                'CustomsLines'  => [],
            ];

            $totalValue = 0.00;

            foreach ($request->getCustomsItems() as $item) {
                $labelRequest['Customs']['CustomsLines'][] = [
                    'Description'     => substr($item->getDescription(), 0, 60),
                    'Quantity'        => $item->getQuantity(),
                    'Value'           => $item->getValue(),
                    'WeightLb'        => $item->getWeight(),
                    'CountryOfOrigin' => 'US',
                ];

                $totalValue += $item->getValue();
            }

            $labelRequest['Rate']['DeclaredValue'] = $totalValue;
        }

        $result = $this->CreateIndicium($labelRequest);

        if ($result instanceof \SoapFault) {
            throw LabelClientException::clientError($result->faultstring);
        }

        return $result;
    }
}

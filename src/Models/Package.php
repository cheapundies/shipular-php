<?php

namespace Shipular\Models;

class Package
{
    /**
     * @var string
     */
    private $memo;

    /**
     * @var float
     */
    private $weight;

    /**
     * @var float
     */
    private $length;

    /**
     * @var float
     */
    private $width;

    /**
     * @var float
     */
    private $height;

    /**
     * Package constructor.
     *
     * @param string  $memo
     * @param float    $weight
     * @param float   $length
     * @param float   $width
     * @param float   $height
     */
    public function __construct(string $memo, float $weight, float $length, float $width, float $height)
    {
        $this->memo   = $memo;
        $this->weight = $weight;
        $this->length = $length;
        $this->width  = $width;
        $this->height = $height;
    }

    /**
     * @return string
     */
    public function getMemo(): string
    {
        return $this->memo;
    }

    /**
     * @return float
     */
    public function getWeight(): float
    {
        return $this->weight;
    }

    /**
     * @return float
     */
    public function getLength(): float
    {
        return $this->length;
    }

    /**
     * @return float
     */
    public function getWidth(): float
    {
        return $this->width;
    }

    /**
     * @return float
     */
    public function getHeight(): float
    {
        return $this->height;
    }
}
